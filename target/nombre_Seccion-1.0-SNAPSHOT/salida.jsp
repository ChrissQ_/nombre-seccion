
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nombre y Seccion</title>
    </head>
    <body>
        <h1>Nombre y Sección</h1>
        
        <% 
            String nombre = (String) request.getAttribute("name");
            String seccion = (String) request.getAttribute("section");
        %>
        <p>Hola <%=nombre%>, tu sección es...</p>
        <p>Seccion: <%=seccion%></p>
    </body>
</html>
